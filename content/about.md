---
title: "About"
description: "About Nexus"
date: 2020-08-29T02:02:04+05:30
author: "false"
tags: ["about"]
draft: false
url: about
---

## What is Nexus SJCET?
Nexus SJCET is a free and open-source software and hardware club. Nexus exists
for people with various interests to come together as a community to learn,
engineer and build as a community. Nexus was formed on 12 September, 2018
by a group of students to support and organize Free/Libre and Open-Source
Software and Hardware related activities in SJCET, Palai.

## Mission
The mission of Nexus SJCET is to act as a true hacker's forge, where
technologically individuals can work together for the betterment of the society
and themselves.

## Vision
Our vision is to be a place where a lot of humans are able to learn and help
each other grow by sharing knowledge and resources in the true spirit of humanity.

## Guiding Principles
A club like nexus is essentially an idealistic organization rather than an opportunistic
one. The guiding principles that drive the activities at Nexus are:
- [Hacker Culture](https://en.wikipedia.org/wiki/Hacker_culture) (Do not confuse
  a hacker with a [cracker](http://www.catb.org/jargon/html/C/cracker.html))
- People Prime: The club and the community around it exists for the people in it
  and not the other way round.
- [Human
  Rights](https://www.un.org/en/about-us/universal-declaration-of-human-rights),
  [Freedom](https://en.wikipedia.org/wiki/Four_Freedoms) and [Equity](https://onlinepublichealth.gwu.edu/resources/equity-vs-equality/).

## Governance
We govern ourselves. Nexus is very much community driven and decisions are taken
together. However a club committee which is a group consisting of the Club's
official Staff Co-ordinator and a group to manage the community. The club
committee is responsible for organizing polls for making decisions relating to
the club that the community can vote in, for getting relevant permissions to
organize various events, to keep track of the functioning of the club.
